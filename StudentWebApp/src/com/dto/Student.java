package com.dto;

public class Student {
	

public class Employee {
	private int stdId;
	private String stdName;
	private String gender;
	private String emailId;
	private String password;
	
	public Employee() {
		super();
	}

	public Employee(int empId, String empName, double salary, String gender, String emailId, String password) {
		super();
		this.stdId = stdId;
		this.stdName = stdName;
		this.gender = gender;
		this.emailId = emailId;
		this.password = password;
	}

	public int getStdId() {
		return stdId;
	}
	public void setStdId(int stdId) {
		this.stdId = stdId;
	}

	public String getStdName() {
		return stdName;
	}
	public void setStdName(String stdName) {
		this.stdName = stdName;
	}

	

	public String getGender() {
		return gender;
	}
	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getEmailId() {
		return emailId;
	}
	public void setEmailId(String emailId) {
		this.emailId = emailId;
	}

	public String getPassword() {
		return password;
	}
	public void setPassword(String password) {
		this.password = password;
	}

	@Override
	public String toString() {
		return "Employee [stdId=" + stdId + ", stdName=" + stdName + ",gender=" + gender
				+ ", emailId=" + emailId + ", password=" + password + "]";
	}
}

}
