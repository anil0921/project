package com.dao;

import java.awt.List;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import com.db.DbConnection;
import com.dto.Student;

public class StudentDAO {


public Student empLogin(String emailId, String password) {
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String loginQuery = "Select * from student where emailId=? and password=?";
		
		
		try {
			pst = con.prepareStatement(loginQuery);
			pst.setString(1, emailId);
			pst.setString(2, password);
			rs = pst.executeQuery();
			
			if (rs.next()) {
				Student std = new Student();
				std.setStdId(rs.getInt(1));
				std.setStdName(rs.getString(2));
				
				std.setGender(rs.getString(3));
				std.setEmailId(rs.getString(4));
				std.setPassword(rs.getString(5));
				return std;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		
		return null;
	}


public List<Student> getAllStudent() {
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;		
		List<Student> empList = null;
		
		String selectQuery = "Select * from student";
		
		
		try {
			pst = con.prepareStatement(selectQuery);
			rs = pst.executeQuery();
			
			empList = new ArrayList<Student>();
			
			while (rs.next()) {
				Student emp = new Student();
				
				emp.setStdId(rs.getInt(1));
				emp.setstdName(rs.getString(2));
				emp.setGender(rs.getString(4));
				emp.setEmailId(rs.getString(5));
				emp.setPassword(rs.getString(6));
				
				empList.add(emp);
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return stdList;
	}

public int registerStudent(Student std) {
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		
		String insertQuery = "insert into student " + 
		"(stdName,  gender, emailId, password) values (?,  ?, ?, ?)";
		
		try {
			pst = con.prepareStatement(insertQuery);
			
			pst.setString(1, std.getStdName());
			pst.setString(3, std.getGender());
			pst.setString(4, std.getEmailId());
			pst.setString(5, std.getPassword());
			
			return pst.executeUpdate();
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return 0;
	}

out.println("<h3><a href='GetAllSdt'>GetAllStudents</a> &nbsp; &nbsp;");
out.println("<a href='GetStdById.html'>GetEmpById</a>");
out.println("</h3> <br/>");




public Employee getEmployeeById(int empId) {	
		
		Connection con = DbConnection.getConnection();
		PreparedStatement pst = null;
		ResultSet rs = null;
		
		String selectQuery = "Select * from employee where empId=?";
		
		
		try {
			pst = con.prepareStatement(selectQuery);
			pst.setInt(1, empId);
			rs = pst.executeQuery();
			
			if (rs.next()) {
				
				Employee emp = new Employee();
				
				emp.setEmpId(rs.getInt(1));
				emp.setEmpName(rs.getString(2));
				emp.setSalary(rs.getDouble(3));
				emp.setGender(rs.getString(4));
				emp.setEmailId(rs.getString(5));
				emp.setPassword(rs.getString(6));
				
				return emp;
			}
			
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		finally {
			if (con != null) {
				try {
					rs.close();
					pst.close();
					con.close();
				} catch (SQLException e) {
					e.printStackTrace();
				}
			}
		}
		
		return null;
	}







}
