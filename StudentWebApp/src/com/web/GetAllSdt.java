package com.web;

import java.io.IOException;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class GetAllSdt
 */
@WebServlet("/GetAllSdt")
public class GetAllSdt extends HttpServlet {
	private static final long serialVersionUID = 1L;
       
    /**
     * @see HttpServlet#HttpServlet()
     */
    public GetAllSdt() {
        super();
        // TODO Auto-generated constructor stub
    }

	/**
	 * @see HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		response.setContentType("text/html");
		PrintWriter out = response.getWriter();
		
		StudentDAO stdDao = new StudentDAO();		
		List<Student> empList = stdDao.getAllStudent();
		
		RequestDispatcher rd = request.getRequestDispatcher("AdminHomePage");
		rd.include(request, response);
		
		out.println("<center>");
		
		if (empList != null) {
			
			out.println("<table border=2>");
			
			out.println("<tr>");
			out.println("<th>StdId</th>");
			out.println("<th>StdName</th>");
			out.println("<th>Gender</th>");
			out.println("<th>Email-Id</th>");
			out.println("</tr>");
			
			for (Employee emp : empList) {
				out.println("<tr>");
				out.println("<td>" + emp.getStdId()   + "</td>");
				out.println("<td>" + emp.getStdName() + "</td>");
				out.println("<td>" + emp.getGender()  + "</td>");
				out.println("<td>" + emp.getEmailId() + "</td>");
				out.println("</tr>");
			}
			
			out.println("</table>");			
		
		} else {			
			out.println("<h1 style='color:red;'>Unable to Fetch the Student Record(s)!!!</h1>");	
		}
		out.println("</center>");
	}
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
		doGet(request, response);
	}

}
